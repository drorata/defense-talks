\documentclass{beamer}

\usepackage{multimedia}
\usepackage[notes=hide]{defense-talks}

\author[D. Atariah]{Dror Atariah}
\title[PhD. Defense]{Parameterizations in the Configuration Space and Approximations of Related Surfaces}
\date{May 9\textsuperscript{th}, 2014}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\section*{Introduction}

\begin{frame}<1>[label=outline]
  \begin{center}
    \begin{tikzpicture}
      \tikzset{arcednodestyle/.style={draw=black,fill=gray!20,very thick}}
      \node<1->[%
        circle,white,draw=black,fill=black!70,%
        very thick,align=center,%
        minimum size=3cm] at (0,0) {Motion\\ Planning};
        \onslide<2->{\arcednode{2}{3.5}{140}{40} {arcednodestyle}{Parameterization}}
        \onslide<3->{\arcednode{2}{3.5}{160}{260}{arcednodestyle}{Approximation}}
        \onslide<4->{\arcednode{2}{3.5}{280}{380}{arcednodestyle}{Arrangements}}
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Motion Planning Problem}
  \begin{center}
    \input{./tikz-figures/generic-wspace-example}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Boundary between \cfree{} and \cforb{}}
  \begin{center}
    \input{./tikz-figures/contact-types-example}
  \end{center}
\end{frame}

\section*{Parameterization}
\againframe<2>{outline}

\begin{frame}
  \frametitle{Vertex-Edge}
  \begin{minipage}{.5\linewidth}
    \begin{center}
      \movie[autostart,palindrome]{%
        \includegraphics[width=\linewidth]{ve-contact-example-standalone}}{%
        ve-contact-example-standalone.avi}
    \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}{.45\linewidth}
    \begin{center}
      \includegraphics[width=0.7\linewidth]{./figures/ve-surface-example}
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Edge-Vertex}
  \begin{minipage}{.5\linewidth}
    \begin{center}
      \movie[autostart,palindrome]{%
        \includegraphics[width=\linewidth]{ev-contact-example-standalone}}{%
        ev-contact-example-standalone.avi}
    \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}{.45\linewidth}
    \begin{center}
      \includegraphics[width=0.7\linewidth]{./figures/ev-surface-example}
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Video}
  \movie[autostart,palindrome,width=\textwidth,height=0.5625\textwidth]{%
    \includegraphics[width=\textwidth,height=0.5625\textwidth]{cspace-vis-image}}%
  {cspace-vis.avi}
\end{frame}

\section*{Approximation}
\againframe<3>{outline}

\begin{frame}
  \frametitle{Motivation}
  \begin{center}
    \only<1>{
      \begin{tikzpicture}
        \node[anchor=west] (ev-example) at (0,0) {%
          \includegraphics[scale=0.2,trim=5cm 1cm 7cm 5.7cm,clip=true]{%
            ev-contact-example-standalone}};%
        \draw[%
        decorate,decoration={snake,pre length=10pt,post length=10pt},->,shorten >=2pt,thick] %
        (ev-example.east) -- ++(0:2cm) coordinate (arrow-tip);
        \node[anchor=west] (ev-contact-surface) at (arrow-tip.east) {%
          \includegraphics[trim=5.5cm 3cm 3.2cm 2cm,clip=true,height=0.7\textheight]{%
            ./figures/ev-surface-example}};%
      \end{tikzpicture}
    }
    \only<2-3>{
      \begin{tikzpicture}
        \node[anchor=west] (ev-contact-surface) at (0,0) {%
          \includegraphics[trim=5.5cm 3cm 3.2cm 2cm,clip=true,height=0.7\textheight]{%
            ./figures/ev-surface-example}};%
        \onslide<3->{\draw[%
          decorate,decoration={snake,pre length=10pt,post length=10pt},->,shorten >=2pt,thick] %
          (ev-contact-surface.east) -- ++(0:2cm) coordinate (arrow-tip);%
          \begin{scope}[xshift=6.75cm,yshift=-1.5cm]
            \begin{axis}[%
              scale=0.7,grid=major,colormap/greenyellow,xtick=\empty,ytick=\empty,ztick=\empty]%
              \addplot3[surf,shader=faceted] {x*y};%
            \end{axis}%
          \end{scope}%
        }
      \end{tikzpicture}
    }
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Approximation of Saddles}
  \begin{block}{Goal}
    Obtain a local and optimal (w.r.t. the \emph{vertical distance}) approximation of saddle surfaces.
  \end{block}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{./figures/v-dist-example}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \only<1>{
    \begin{block}{Interpolating}
      \begin{center}
        \includegraphics[width=0.6\linewidth]{./asymptote/inter-saddle-tri}
      \end{center}
    \end{block}
  }
  \only<2>{
    \begin{theorem}[Non-Interpolating]
      \label{th:opt-saddle-tri:set-non-interpolating-triangle}
      Let \(\epsilon>0\) be fixed and set \(T_\star  \subset \R^2\) to be the triangle with the following vertices: %
      \[%
      \begin{gathered}
        p_0  = \left( 0,0 \right)\\
        p_1  = 2\sqrt\epsilon \left( 1 + \frac{1}{\sqrt 3} ,  1 - \frac{1}{\sqrt 3} \right) ,
        p_2  = 2\sqrt \epsilon \left(1 - \frac{1}{\sqrt 3} , 1 + \frac{1}{\sqrt 3} \right)
      \end{gathered}
      \]%
      If \(\hat{T}_\star\) is the lifting of the triangle \(T_\star\) to the offset saddle \(S_{\frac{\epsilon}{3}}\), then the following hold
      \begin{itemize}
      \item \emph{Vertical distance:} \(\mathrm{dist}_V(S,\hat{T}_\star) = \epsilon\)
      \item \emph{Area of projection to the plane:}
        \[\mathrm{area} (T_\star) = \frac{8\epsilon}{\sqrt 3} \approx 4.6188 > 4.4721 \approx 2 \sqrt 5 \epsilon = \mathrm{area}(T_\star^I)\]
      \end{itemize}
    \end{theorem}
  }
\end{frame}

\section*{\texttt{CGAL}}
\againframe<4>{outline}

\begin{frame}
  \frametitle{Arrangements of Polylines}
  \begin{block}{Motivation}
    \begin{itemize}
    \item Arrangements of rational curves
    \item Extend the support of \emph{2D Arrangements} to unbounded polylines
    \end{itemize}
  \end{block}
  \begin{block}{Contribution}
    \begin{itemize}
    \item Improvement of the implementation
    \item 5\% speedup of the runtime
    \item Code is more generic
    \end{itemize}
  \end{block}
\end{frame}


\section*{Summary}
\againframe<4>{outline}


\appendix
\AtBeginSection[]
{\setbeamercolor{section in toc}{bg=normal text.bg,fg=normal text.fg}
  \begin{frame}<beamer>
    \frametitle{Outline of Appendix}
    \tableofcontents[currentsection]
  \end{frame}
}

\section{Parameterization}
\begin{frame}[fragile]
  \frametitle{Rotating Robot}
  \begin{center}
    \input{./tikz-figures/cspace-para-rotating_about_bound_pt}
  \end{center}
  \begin{theorem}
    Let \(P_a = \left\{\, \cp{q} \in \cspace : a(\cp{q}) = P \,\right\} \), then \(\cp{q} \in P_a\) iff %
    \[%
    q =
    \begin{pmatrix}
      P - R^\phi a \\
      \phi
    \end{pmatrix}
    \]%
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Vertex-Edge Contact}
  \begin{center}
    \input{./tikz-figures/cspace-para-ve-contact}
  \end{center}
  \[%
  S(t,\phi) =
  \begin{pmatrix}
    P(t) - R^\phi a_i\\
    \phi
  \end{pmatrix}
  \]%
\end{frame}

\begin{frame}
  \frametitle{Edge-Vertex Contact}
  \begin{center}
    \input{./tikz-figures/cspace-para-ev-contact}
  \end{center}
  %
  \[%
  S(t,\phi) =
  \begin{pmatrix}
    b_j - R^\phi a_{i,t} \\
    \phi
  \end{pmatrix}
  \]%
\end{frame}

\begin{frame}
  \frametitle{Differential Geometry of E-V Case}
  \begin{itemize}
  \item Surface Normal: \(N_s(t,\phi) = \begin{pmatrix}R^\phi n_i^\robot \\ \left< a_{i,t}, E_i^\robot\right>\end{pmatrix}\)
  \item First Fundamental Forms: \(E = \|a_i - a_{i+1}\|^2\), \(F = \det(a_i,a_{i+1})\) and \(G = 1 + \|a_{i,t}\|^2\)
  \item Gaussian Curvature: \(K(t) =- \frac{E^2}{\nu^4}\), where \(\nu = \sqrt{EG - F^2}\).
  \item Mean Curvature: \(H(t) = \frac{EF}{2\nu^3}\)
  \item \(\sup_{t\in\R}|K(t)| = |K(t_\star)| = 1\) where \(t_\star = \frac{\left< a_i, a_i - a_{i+1}\right>}{E}\) and \(a_{i,t_\star}\) is the closest point on the support line of \(E_i^\robot\) to the origin
  \item Normal Curvature, Principal Curvatures and Principal Curvature Directions
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rational Parameterization}
  %
  \[%
  R^\theta = \frac{1}{1+\tau^2}
  \begin{pmatrix}
    1-\tau^2 & -2\tau \\
    2\tau &
    1-\tau^2
  \end{pmatrix} = M^\tau
  \]%
  for \(\tau = \tan \frac{\theta}{2} \in (-\infty, +\infty)\).
\end{frame}

\section{Approximation}

\begin{frame}
  \frametitle{The Vertical Distance}
  \begin{itemize}
  \item For \(p,q \in S = \left\{\, (x,y,z) : z = F(x,y) \,\right\}\) we have
    \[\mathrm{dist}_V(\ell_{pq},S) = \frac{1}{4}| a_{11}\Delta_x^2 + 2 a_{12} \Delta_x\Delta_y + a_{22} \Delta_y^2|,\]
    where \(\Delta_x = p^x - q^x, \Delta_y = p^y - q^y\) and \(F(x,y)\) is a quadratic form.
  \item \(\mathrm{dist}_H(A,B) \leq \mathrm{dist}_V(A,B)\)

    \begin{center}
      \input{./tikz-figures/saddle-v-dist-bound}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Simple Saddle (Interpolating)}
  For
  \[
  S = \left\{\, (x,y,z) \in \R^3 : z = xy \,\right\}
  \]
  we have:%
  \[%
  \mathrm{dist}_V(S,\ell_{pq}) = \frac{1}{4}| \Delta_x \Delta_y|
  \]%
  \begin{center}
    \only<1-5>{\input{./tikz-figures/saddle-simple-const}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Going Global}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{./figures/saddle-inter-global-tri}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Construction of non-interpolating case}
  \begin{center}
    \input{./tikz-figures/saddle-non-inter-base}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Interpolating vs. non-interpolating}
  \begin{center}
    \includegraphics[width=1\linewidth]{./figures/inter+non-inter-triangles-in-space}
  \end{center}
\end{frame}

\section{\texttt{CGAL}}
\begin{frame}
  \frametitle{TBA}

\end{frame}

\end{document}