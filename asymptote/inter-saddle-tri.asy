import grid3;
import graph3;
import three;

size(10cm);

currentprojection=perspective(20,-30,15);
currentlight=(20,-20,10);

// Functions

triple lift(pair p, real h) {
  // Vertical lift of a 2D point to hight h
  return (p.x,p.y,h);
}

real b=1.0; // b parameter of the hyperbola
real f(pair z) {// The saddle's function
  return 0.5*(z.x^2-b*z.y^2);
}

real vertexRad=0.04;
void drawT(triple a, triple b, triple c,pen col) {
  // Fill and draw a spatial triangle.
  // In addition, mark its vertices
  draw(surface(a--b--c--cycle),col);
  draw(a--b--c--cycle,black+2);
  draw(shift(a)*scale3(vertexRad)*unitsphere,black);
  draw(shift(b)*scale3(vertexRad)*unitsphere,black);
  draw(shift(c)*scale3(vertexRad)*unitsphere,black);
}

// Constants:
real e=.1; // The error
pair minBnd=(-1.5,-1.2); // (x,y) min values
pair maxBnd=(1.5,1.2); // (x,y) max values
real floor=f((0,minBnd.y))-0.4; // The hight of the floor
pair translateVec=(-0.25,-.3); // Translate the center of the approximation
// pair translateVec=(0,0);

pair[] p; // planar points
p.push((0,0)+translateVec);
// Set the vertices of T
p.push((sqrt(5*e),-sqrt(e/b))+translateVec);
p.push((sqrt(5*e),sqrt(e/b))+translateVec);
p.push(p[2]+p[0]-p[1]);
p.push(p[3]+p[0]-p[2]);
p.push(p[4]+p[0]-p[3]);
p.push(p[5]+p[0]-p[4]);

triple q[]; // 3D points on the floor
triple Q[]; // 3D points on the saddle
for (int i=0; i < 7 ; ++i)
  {
    q.push(lift(p[i],floor));
    Q.push(lift(p[i],f(p[i])));
  }

// Plot the saddle
draw(surface(f,minBnd,maxBnd,nx=10,Spline),gray+opacity(0.95));

pen[] colors={red,green,blue,0.5*red,0.5*green,0.5*blue};
for (int i=1; i<7 ; ++i)
  {
    if (i < 6) {
      drawT(q[0],q[i],q[i+1%7],colors[i-1]);
      drawT(Q[0],Q[i],Q[i+1%7],colors[i-1]);}
    else {
      drawT(q[0],q[6],q[1],colors[5]);
      drawT(Q[0],Q[6],Q[1],colors[5]);}
  }

// Draw axes frame
triple O=(-1.5,-1.2,floor), X=O+(1,0,0), Y=O+(0,1,0), Z=O+(0,0,1);
draw(O--X,black,Arrow3);
draw(O--Y,black,Arrow3);
draw(O--Z,black,Arrow3);
label("$x$",scale3(1.1)*X,black);
label("$y$",scale3(1.1)*Y+(-0.2,0.3,0.05),black);
label("$z$",scale3(1.1)*Z,black);

// Label and mark p_0
triple labelSource=(1,1,1);
real labelTargetRatio=0.98;
triple labelTarget=labelTargetRatio*Q[0]+(1-labelTargetRatio)*labelSource;
draw(0.75*labelSource -- labelTarget,brown,Arrow3);
label("$\hat{p}_0$",0.75*labelSource+(0,0,0.1),black);
