#!/bin/bash

EXPECTED_ARGS=3
E_BAD_NUM_ARGS=11

if [ "$#" -ne "$EXPECTED_ARGS" ]; then
    echo "Wrong number of arguments"
    echo "Usage: tex2avi {basename} {density} {frame_rate}"
    echo "{basename} - the file name of the TeX file without an extension"
    echo "{density} - density parameter for the convert PDF->JPGs (recommended: 500)"
    echo "{frame_rate} - the frame rate to be used with ffmpeg (recommended: 10)"
    exit $E_BAD_NUM_ARGS
fi

BASENAME=$1
TEXNAME="$BASENAME.tex"
PDFNAME="$BASENAME.pdf"

# Generate the PDF from the source TeX file
pdflatex $TEXNAME

echo "Converting PDF to a sequence of JPGs"
convert -density $2 $PDFNAME "$BASENAME"_%03d.jpg

echo "Generating the video"
ffmpeg -y -r $3 -i "$BASENAME"_%03d.jpg "$BASENAME".avi

echo "Keep first frame"
cp "$BASENAME"_001.jpg "$BASENAME".jpg

echo "Clean..."
rm -f "$BASENAME"_*.jpg
rm -f "$BASENAME".log
rm -f "$BASENAME".aux
rm -f "$BASENAME".pdf
